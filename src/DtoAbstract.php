<?php

namespace Qstione\Dto;

use Carbon\Carbon;

abstract class DtoAbstract implements DtoInterface
{
    /** @var string  */
    public const DATA_INVALIDA = '0000-00-00';

    /** @var int|null */
    protected $id;

    /** @var bool */
    protected $toSql;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id): void
    {
        $id = (int) $id;

        $this->id = $id > 0 ? $id : null;
    }

    /**
     * @param array|null $values
     */
    public function __construct(?array $values = [])
    {
        foreach ($values as $key => $value) {
            if (property_exists($this, $key)) {
                $this->setValue($key, $value);
            }
        }

        $this->autoFill();
    }

    /**
     * @param string $key
     * @param $value
     * @return null
     */
    public function setValue(string $key, $value)
    {
        $function = 'set';

        array_map(function ($word) use (&$function) {
            $function .= ucfirst($word);
        }, explode('_', $key));

        if (method_exists($this->traceClass(), $function)) {
            $this->$function($value);

            return null;
        }

        $this->$key = $value;

        return null;
    }

    /**
     * @param string $key
     * @return null
     */
    public function getValue(string $key)
    {
        $function = 'get';

        array_map(function ($word) use (&$function) {
            $function .= ucfirst($word);
        }, explode('_', $key));

        if (method_exists($this->traceClass(), $function)) {
            return $this->getValueOrDto($this->$function());
        }

        return $this->getValueOrDto($this->$key);
    }

    /**
     * @param $value
     * @return array|int|null
     */
    public function getValueOrDto($value)
    {
        if ($value instanceof DtoAbstract) {
            return $this->toSql
                ? $value->getId()
                : $value->toArray(true);
        }

        return $value !== "" ? $value : null;
    }

    /**
     * @return false|string
     */
    public function traceClass()
    {
        return get_class(debug_backtrace()[0]['object']);
    }

    /**
     * @param bool|null $toSql
     * @return array
     */
    public function toArray(?bool $toSql = false): array
    {
        $return      = [];
        $vars        = get_object_vars($this);
        $this->toSql = $toSql;

        array_map(function ($key) use (&$return) {
            if (in_array($key, $this->excludeFields())) {
                return null;
            }

            $return[$key] = $this->getValue($key);
        }, array_keys($vars));

        return $return;
    }

    /**
     * @return array
     */
    public function excludeFields(): array
    {
        return ['toSql'];
    }

    /**
     * @return void
     */
    public function autoFill(): void
    {
        $autoFills = $this->autoFills();

        if (empty($autoFills)) {
            return;
        }

        array_map(function ($key) {
            $this->setValueAutoFill($key);
        }, $autoFills);
    }

    /**
     * @return array
     */
    public function autoFills(): array
    {
        return [];
    }

    /**
     * @param string $key
     */
    public function setValueAutoFill(string $key)
    {
        $function = 'set';

        array_map(function ($word) use (&$function) {
            $function .= ucfirst($word);
        }, explode('_', $key));

        if (method_exists($this->traceClass(), $function)) {
            $this->$function();
        }
    }

    /**
     * @param $valor
     * @return array|string
     */
    public function formatToSetMultiOptions($valor)
    {
        if ($this->toSql) {
            return $this->implodeMultiOptions($valor);
        }

        return $this->explodeMultiOptions($valor);
    }


    /**
     * @param string $campo
     * @return array|string
     */
    public function formatToGetMultiOption(string $campo)
    {
        if ($this->toSql) {
            return $this->implodeMultiOptions($this->$campo);
        }

        return $this->explodeMultiOptions($this->$campo);
    }

    /**
     * @param $value
     * @return array|null
     */
    public function explodeMultiOptions($value): ?array
    {
        $arrayCampoRetorno = [];

        if (!$value || is_array($value)) {
            return $value;
        }

        array_map(function ($val) use (&$arrayCampoRetorno) {
            $option = explode(":", $val);
            $arrayCampoRetorno[$option[0]] = $option[1];
        }, array_filter(explode(',', $value)));

        return $arrayCampoRetorno;
    }

    /**
     * @param $value
     * @return string|null
     */
    public function implodeMultiOptions($value): ?string
    {
        if (empty($value) || !is_array($value)) {
            return $value;
        }

        $valorString = '';

        array_map(function ($val, $key) use (&$valorString) {
            $valorString .= $key . ":" . $val . ",";
        }, $value, array_keys($value));

        return $valorString;
    }

    /**
     * @return false|string
     */
    public function toJson(): string
    {
        return json_encode($this->toArray(true), true);
    }

    /**
     * @param $number
     * @return string|null
     */
    public function toNumber($number): ?string
    {
        if (is_null($number) || "" === $number) {
            return null;
        }

        if ($this->toSql) {
            return \String_Refatorar::parseStringToFloat($number);
        }

        return number_format($number, 2, ',', '.');
    }

    /**
     * @param string|null $date
     * @return string|null
     */
    public function dateFormat(?string $date): ?string
    {
        if (strpos($date, '-') && $this->toSql) {
            return $date;
        }

        $time = '';

        if (strpos($date, ':')) {
            $time    = $this->toSql ? ' H:i' : ' H:i:s';
            $newTime = $this->toSql ? ' H:i:s' : ' H:i';
        }

        $format       = $this->toSql ? 'd/m/Y' : 'Y-m-d';
        $newFormat    = $this->toSql ? 'Y-m-d' : 'd/m/Y';

        return $date && $date !== self::DATA_INVALIDA
            ? Carbon::createFromFormat($format . $time, $date)->format($newFormat . $newTime)
            : '';
    }
}
