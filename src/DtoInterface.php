<?php

namespace Qstione\Dto;

interface DtoInterface
{
    public function __construct(array $values);

    /**
     * @param string $key
     * @param $value
     * @return mixed
     */
    public function setValue(string $key, $value);

    /**
     * @param string $key
     * @return null
     */
    public function getValue(string $key);

    /**
     * @return false|string
     */
    public function traceClass();

    /**
     * @return array
     */
    public function toArray(): array;

    /**
     * @return array
     */
    public function excludeFields(): array;
}
